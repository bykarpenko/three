const FtpDeploy = require("ftp-deploy");
const ftpDeploy = new FtpDeploy();

// here you can setup name for project on FTP. For example "my-project-v2"
const folderName = __dirname.split('/').slice(-1)[0];

const ftpPath = "/todorov3d.tech/www/webgl/" + folderName;
const productionLink = "http://todorov3d.tech/webgl/" + folderName;

const config = {
    user: "todorov3_ftp",
    password: "X4V3sSCk",
    host: "todorov3.ftp.tools",
    port: 21,
    localRoot: __dirname + '/dist',
    remoteRoot: ftpPath,
    include: ["*"],
    // here you can files that will not be pushed to ftp. For example "icons/*"
    exclude: [],
    // delete ALL existing files at destination before uploading, if true
    deleteRemote: true,
    // Passive mode is forced (EPSV command is not sent)
    forcePasv: true
};

ftpDeploy.on("uploading", (data) => {
    console.log("Transferred: ", data.transferredFileCount + 1 + '/' + data.totalFilesCount, ' Filename: ' + data.filename);
});

ftpDeploy.on("upload-error", function (data) {
    console.log(data.err);
});

ftpDeploy
    .deploy(config)
    .then(res => console.log("All files uploaded. T3D production presents: " + productionLink))
    .catch(err => console.log(err));

